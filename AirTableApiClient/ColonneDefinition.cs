﻿using System.Text.Json.Serialization;

namespace AirTableApiClient;

/// <summary>
/// Classe qui contient la définition d'une colonne
/// </summary>
public class ColonneDefinition
{
    [JsonPropertyName("name")]
    public string Nom { get; set; }

    [JsonConverter(typeof(JsonStringEnumConverter))]
    [JsonPropertyName("type")]
    public TypeColonneEnum Type { get; set; }

    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    [JsonPropertyName("options")]
    public Dictionary<string, object>? Options { get; set; }

    #region Methodes de création
    /// <summary>
    /// Créer une colonne de type date au format yyyy-MM-dd
    /// </summary>
    /// <param name="nom">Nom de la colonne</param>
    /// <returns>La définition de la colonne</returns>
    public static ColonneDefinition CreerDate(string nom)
    {
        ColonneDefinition colonne = CreerColonne(nom, TypeColonneEnum.date);

        colonne.Options = new Dictionary<string, object>();

        Dictionary<string, object> dicDate = new Dictionary<string, object>();
        dicDate.Add("name", "iso");

        colonne.Options.Add("dateFormat", dicDate);

        return colonne;
    }

    /// <summary>
    /// Créer une colonne de type Checkbox
    /// </summary>
    /// <param name="nom">Nom de la colonne</param>
    /// <returns>La définition de la colonne</returns>
    public static ColonneDefinition CreerCheckbox(string nom)
    {
        ColonneDefinition colonne = CreerColonne(nom, TypeColonneEnum.checkbox);

        colonne.Options = new Dictionary<string, object>();

        colonne.Options.Add("color", "greenBright");
        colonne.Options.Add("icon", "check");

        return colonne;
    }

    /// <summary>
    /// Créer une colonne de type Number
    /// </summary>
    /// <param name="nom">Nom de la colonne</param>
    /// <param name="precision">La précision du nombre. 0 est pour un entier</param>
    /// <returns>La définition de la colonne</returns>
    public static ColonneDefinition CreerNumber(string nom, int precision)
    {
        ColonneDefinition colonne = CreerColonne(nom, TypeColonneEnum.number);

        colonne.Options = new Dictionary<string, object>();

        colonne.Options.Add("precision", precision);

        return colonne;
    }

    /// <summary>
    /// Créer une colonne de type SingleLineText
    /// </summary>
    /// <param name="nom">Nom de la colonne</param>    
    /// <returns>La définition de la colonne</returns>
    public static ColonneDefinition CreerSingleLineText(string nom)
    {
        return CreerColonne(nom, TypeColonneEnum.singleLineText);
    }

    private static ColonneDefinition CreerColonne(string nom, TypeColonneEnum typeColonne)
    {
        return new ColonneDefinition()
        {
            Nom = nom,
            Type = typeColonne
        };
    }
    #endregion
}
