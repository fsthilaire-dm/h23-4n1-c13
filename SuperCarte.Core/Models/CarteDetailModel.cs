﻿namespace SuperCarte.Core.Models;

/// <summary>
/// Classe qui contient l'information d'une carte avec le détail de ses clés étrangères
/// </summary>
public class CarteDetailModel : CarteModel
{
    public string CategorieNom { get; set; } = null!;

    public string EnsembleNom { get; set; } = null!;
}
