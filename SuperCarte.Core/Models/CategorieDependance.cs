﻿using SuperCarte.EF.Data;

namespace SuperCarte.Core.Models;

/// <summary>
/// Classe qui contient le nombre de dépendances pour une catégorie
/// </summary>
public class CategorieDependance
{
    public int CategorieId { get; init; }
    public int NbCartes { get; init; }
}
