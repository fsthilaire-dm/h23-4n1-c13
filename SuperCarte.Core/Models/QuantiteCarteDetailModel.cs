﻿namespace SuperCarte.Core.Models;

/// <summary>
/// Classe qui contient l'information complète d'une carte et avec une quantité
/// </summary>
public class QuantiteCarteDetailModel : CarteDetailModel
{
    public short Quantite {  get; set; }
}
