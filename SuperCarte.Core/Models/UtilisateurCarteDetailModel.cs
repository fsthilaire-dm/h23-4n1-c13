﻿namespace SuperCarte.Core.Models;

/// <summary>
/// Classe qui contient l'information d'une carte d'un utilisateur avec le détail de ses clés étrangères
/// </summary>
public class UtilisateurCarteDetailModel : UtilisateurCarteModel
{
    public string UtilisateurNom { get; set; } = null!;

    public string UtilisateurPrenom { get; set; } = null!;

    public string CarteNom { get; set; } = null!;

    public string CategorieNom { get; set; } = null!;

    public string EnsembleNom { get; set; } = null!;
}
