﻿namespace SuperCarte.Core.Models;

/// <summary>
/// Classe qui contient l'information d'une carte d'un utilisateur
/// </summary>
public class UtilisateurCarteModel
{
    public int UtilisateurId { get; set; }
    public int CarteId { get; set; }
    public short Quantite { get; set; }
}