﻿using Microsoft.EntityFrameworkCore;
using SuperCarte.EF.Data.Context;

namespace SuperCarte.Core.Repositories.Bases;

/// <summary>
/// Classe abstraite générique qui contient les opérations de base des tables de la base de données
/// </summary>
/// <typeparam name="TData">Type du modèle de données / table</typeparam>
public class BaseRepo<TData> : IBaseRepo<TData> where TData : class
{
    protected readonly SuperCarteContext _bd;

    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="bd">Contexte de la base de données</param>
    public BaseRepo(SuperCarteContext bd)
    {
        _bd = bd;
    }

    public async Task<List<TData>> ObtenirListeAsync()
    {
        //Équivalent à _db.TData.ToListAsync();
        return await _bd.Set<TData>().ToListAsync();
    }

    public List<TData> ObtenirListe()
    {
        //Équivalent à _db.TData.ToList();
        return _bd.Set<TData>().ToList();
    }

    public async Task AjouterAsync(TData item, bool enregistrer)
    {
        //Add est déjà générique.
        //Sa définition réelle est _bd.Add<TData>().
        _bd.Add(item);

        //Vérifie si l'ajout doit être appliqué dans la base de données immédiatement
        if (enregistrer == true)
        {
            //L'ajout doit être appliqué dans la base de données immédiatement
            await _bd.SaveChangesAsync();
        }
        else
        {
            //L'ajout est seulement en mémoire
            await Task.CompletedTask;
        }
    }

    public void Ajouter(TData item, bool enregistrer)
    {
        //Add est déjà générique.
        //Sa définition réelle est _bd.Add<TData>().
        _bd.Add(item);

        //Vérifie si l'ajout doit être appliqué dans la base de données immédiatement
        if (enregistrer == true)
        {
            //L'ajout doit être appliqué dans la base de données immédiatement
            _bd.SaveChanges();
        }
    }

    public async Task AjouterAsync(List<TData> lstItem, bool enregistrer)
    {
        //AddRange est déjà générique.
        //Sa définition réelle est _bd.AddRange<TData>().
        _bd.AddRange(lstItem);

        //Vérifie si l'ajout doit être appliqué dans la base de données immédiatement
        if (enregistrer == true)
        {
            //L'ajout doit être appliqué dans la base de données immédiatement
            await _bd.SaveChangesAsync();
        }
        else
        {
            //L'ajout est seulement en mémoire
            await Task.CompletedTask;
        }
    }

    public void Ajouter(List<TData> lstItem, bool enregistrer)
    {
        //AddRange est déjà générique.
        //Sa définition réelle est _bd.AddRange<TData>().
        _bd.AddRange(lstItem);

        //Vérifie si l'ajout doit être appliqué dans la base de données immédiatement
        if (enregistrer == true)
        {
            //L'ajout doit être appliqué dans la base de données immédiatement
            _bd.SaveChanges();
        }
    }

    public async Task SupprimerAsync(TData item, bool enregistrer)
    {
        //Remove est déjà générique.
        //Sa définition réelle est _bd.Remove<TData>().
        _bd.Remove(item);

        //Vérifie si la suppression doit être appliquée dans la base de données immédiatement
        if (enregistrer == true)
        {
            //La suppression doit être appliquée dans la base de données immédiatement
            await _bd.SaveChangesAsync();
        }
        else
        {
            //La suppression est seulement en mémoire
            await Task.CompletedTask;
        }
    }

    public void Supprimer(TData item, bool enregistrer)
    {
        //Remove est déjà générique.
        //Sa définition réelle est _bd.Remove<TData>().
        _bd.Remove(item);

        //Vérifie si la suppression doit être appliquée dans la base de données immédiatement
        if (enregistrer == true)
        {
            //La suppression doit être appliquée dans la base de données immédiatement
            _bd.SaveChanges();
        }
    }

    public async Task SupprimerAsync(List<TData> lstItem, bool enregistrer)
    {
        //RemoveRange est déjà générique.
        //Sa définition réelle est _bd.RemoveRange<TData>().        
        _bd.RemoveRange(lstItem);

        //Vérifie si la suppression doit être appliquée dans la base de données immédiatement
        if (enregistrer == true)
        {
            //La suppression doit être appliquée dans la base de données immédiatement
            await _bd.SaveChangesAsync();
        }
        else
        {
            //La suppression est seulement en mémoire
            await Task.CompletedTask;
        }
    }

    public void Supprimer(List<TData> lstItem, bool enregistrer)
    {
        //RemoveRange est déjà générique.
        //Sa définition réelle est _bd.RemoveRange<TData>().        
        _bd.RemoveRange(lstItem);

        //Vérifie si la suppression doit être appliquée dans la base de données immédiatement
        if (enregistrer == true)
        {
            //La suppression doit être appliquée dans la base de données immédiatement
            _bd.SaveChanges();
        }
    }
    public async Task EnregistrerAsync()
    {
        //Enregistre les ajouts, modifications et suppression en attente dans la mémoire du contexte
        await _bd.SaveChangesAsync();
    }

    public void Enregistrer()
    {
        //Enregistre les ajouts, modifications et suppression en attente dans la mémoire du contexte
        _bd.SaveChanges();
    }
}