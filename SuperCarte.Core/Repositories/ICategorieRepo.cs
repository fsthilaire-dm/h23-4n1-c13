﻿using SuperCarte.Core.Models;
using SuperCarte.Core.Repositories.Bases;
using SuperCarte.EF.Data;

namespace SuperCarte.Core.Repositories;

/// <summary>
/// Interface qui contient les méthodes de communication avec la base de données pour la table Categorie
/// </summary>
public interface ICategorieRepo : IBasePKUniqueRepo<Categorie, int>
{
    /// <summary>
    /// Obtenir les dépendances d'une catégorie en asynchrone.
    /// </summary>
    /// <param name="categorieId">Clé primaire de la catégorie/param>
    /// <returns>Les dépendances ou null si la catégorie n'est pas trouvée</returns>
    Task<CategorieDependance?> ObtenirDependanceAsync(int categorieId);

    /// <summary>
    /// Obtenir les dépendances d'une catégorie.
    /// </summary>
    /// <param name="categorieId">Clé primaire de la catégorie/param>
    /// <returns>Les dépendances ou null si la catégorie n'est pas trouvée</returns>
    CategorieDependance? ObtenirDependance(int categorieId);
}
