﻿using SuperCarte.Core.Repositories.Bases;
using SuperCarte.EF.Data;

namespace SuperCarte.Core.Repositories;

/// <summary>
/// Interface qui contient les méthodes de communication avec la base de données pour la table Utilisateur
/// </summary>
public interface IUtilisateurRepo : IBasePKUniqueRepo<Utilisateur, int>
{
    /// <summary>
    /// Obtenir un utilisateur par son nom d'utilisateur en asynchrone
    /// </summary>
    /// <param name="nomUtilisateur">Nom utilisateur</param>    
    /// <returns>L'utilisateur ou null si non trouvé</returns>
    Task<Utilisateur?> ObtenirParNomUtilisateurAsync(string nomUtilisateur);

    /// <summary>
    /// Obtenir le rôle d'un utilisateur en asynchrone
    /// </summary>
    /// <param name="utilisateurId">Utilisateur Id</param>
    /// <returns>Le rôle de l'utilisateur ou null si l'utilisateur est inexistant</returns>
    Task<Role?> ObtenirRoleUtilisateurAsync(int utilisateurId);

    /// <summary>
    /// Obtenir le rôle d'un utilisateur en asynchrone
    /// </summary>
    /// <param name="utilisateurId">Utilisateur Id</param>
    /// <returns>Le rôle de l'utilisateur ou null si l'utilisateur est inexistant</returns>
    Role? ObtenirRoleUtilisateur(int utilisateurId);
}
