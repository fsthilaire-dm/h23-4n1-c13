﻿using AirTableApiClient;
using SuperCarte.Core.Models;
using SuperCarte.Core.Services.AirTable;

namespace SuperCarte.Core.Services;

/// <summary>
/// Classe qui contient les services d'exportation vers AirTable
/// </summary>
public class ExportAirTableService : IExportAirTableService
{
    private readonly IAirTableClient _airTableClient;
    private readonly IUtilisateurCarteService _utilisateurCarteService;

    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="airTableClient">Client pour l'API AirTable</param>
    /// <param name="utilisateurCarteService">Service du modèle UtilisateurCarte</param>
    public ExportAirTableService(IAirTableClient airTableClient, IUtilisateurCarteService utilisateurCarteService)
    {
        _airTableClient = airTableClient;
        _utilisateurCarteService = utilisateurCarteService;
    }

    public async Task<string> ExporterQuantiteCarteDetailModelAsync(int utilisateurId)
    {
        try
        {
            //Doit l'obtenir de l'utilisateur
            string apiJeton = "patRGnAl8Rm1arQei.65b318874bfd92afab130f36d48f64f6ebfa6dedd1d946a604a407d010b43049";
            string baseId = "apptZoczndTWLbJy4";

            if (string.IsNullOrWhiteSpace(apiJeton) == false && string.IsNullOrWhiteSpace(baseId) == false)
            {
                List<QuantiteCarteDetailModel> lstQuantiteCarteDetailModel =
                    await _utilisateurCarteService.ObtenirCartesUtilisateurAsync(utilisateurId);

                //Création du nom de la table par le Ticks
                string tableNom = DateTime.Now.Ticks.ToString();

                //Création de la table
                string tableId = await _airTableClient.CreerTableAsync(apiJeton, baseId, tableNom,
                    ColonneDefinition.CreerNumber(nameof(QuantiteCarteDetailModel.CarteId), 0),
                    ColonneDefinition.CreerSingleLineText(nameof(QuantiteCarteDetailModel.Nom)),
                    ColonneDefinition.CreerNumber(nameof(QuantiteCarteDetailModel.Quantite), 0),
                    ColonneDefinition.CreerNumber(nameof(QuantiteCarteDetailModel.Vie), 0),
                    ColonneDefinition.CreerNumber(nameof(QuantiteCarteDetailModel.Armure), 0),
                    ColonneDefinition.CreerNumber(nameof(QuantiteCarteDetailModel.Attaque), 0),
                    ColonneDefinition.CreerCheckbox(nameof(QuantiteCarteDetailModel.EstRare)),
                    ColonneDefinition.CreerNumber(nameof(QuantiteCarteDetailModel.PrixRevente), 2),
                    ColonneDefinition.CreerNumber(nameof(QuantiteCarteDetailModel.CategorieId), 0),
                    ColonneDefinition.CreerSingleLineText(nameof(QuantiteCarteDetailModel.CategorieNom)),
                    ColonneDefinition.CreerNumber(nameof(QuantiteCarteDetailModel.EnsembleId), 0),
                    ColonneDefinition.CreerSingleLineText(nameof(QuantiteCarteDetailModel.EnsembleNom)));                

                //Ajout de chacun des enregistrements
                foreach (QuantiteCarteDetailModel quantiteCarteDetailModel in lstQuantiteCarteDetailModel)
                {
                    Enregistrement enregistrement = new Enregistrement();

                    enregistrement.AjouterChamp(nameof(QuantiteCarteDetailModel.CarteId), quantiteCarteDetailModel.CarteId);
                    enregistrement.AjouterChamp(nameof(QuantiteCarteDetailModel.Nom), quantiteCarteDetailModel.Nom);
                    enregistrement.AjouterChamp(nameof(QuantiteCarteDetailModel.Quantite), quantiteCarteDetailModel.Quantite);
                    enregistrement.AjouterChamp(nameof(QuantiteCarteDetailModel.Vie), quantiteCarteDetailModel.Vie);
                    enregistrement.AjouterChamp(nameof(QuantiteCarteDetailModel.Armure), quantiteCarteDetailModel.Armure);
                    enregistrement.AjouterChamp(nameof(QuantiteCarteDetailModel.Attaque), quantiteCarteDetailModel.Attaque);
                    enregistrement.AjouterChamp(nameof(QuantiteCarteDetailModel.EstRare), quantiteCarteDetailModel.EstRare);
                    enregistrement.AjouterChamp(nameof(QuantiteCarteDetailModel.PrixRevente), quantiteCarteDetailModel.PrixRevente);
                    enregistrement.AjouterChamp(nameof(QuantiteCarteDetailModel.CategorieId), quantiteCarteDetailModel.CategorieId);
                    enregistrement.AjouterChamp(nameof(QuantiteCarteDetailModel.CategorieNom), quantiteCarteDetailModel.CategorieNom);
                    enregistrement.AjouterChamp(nameof(QuantiteCarteDetailModel.EnsembleId), quantiteCarteDetailModel.EnsembleId);
                    enregistrement.AjouterChamp(nameof(QuantiteCarteDetailModel.EnsembleNom), quantiteCarteDetailModel.EnsembleNom);

                    string enregistrementId = await _airTableClient.CreerEnregistrementAsync(apiJeton, baseId, tableId, enregistrement);
                }

                //Retourne le nom de la table
                return tableNom;
            }
            else
            {
                throw new Exception("Il faut un jeton et un identifiant de base pour l'utilisateur.");
            }
        }
        catch
        {
            //Il y a eu un problème, il n'y a pas de nom.
            return string.Empty;
        }
    }
}