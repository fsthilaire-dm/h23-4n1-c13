﻿namespace SuperCarte.Core.Services.AirTable;

/// <summary>
/// Interface qui contient les services d'exportation vers AirTable
/// </summary>
public interface IExportAirTableService
{
    /// <summary>
    /// Exporter les enregistrements QuantiteCarteDetailModel d'un utilisateur vers Air Table
    /// </summary>
    /// <param name="utilisateurId">Utilisateur</param>
    /// <returns>Le nom autogénéré de la table ou vide s'il y a eu un problème</returns>
    Task<string> ExporterQuantiteCarteDetailModelAsync(int utilisateurId);
}
