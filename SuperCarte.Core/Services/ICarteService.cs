﻿using SuperCarte.Core.Models;

namespace SuperCarte.Core.Services;

/// <summary>
/// Interface qui contient les services du modèle Carte
/// </summary>
public interface ICarteService
{
    /// <summary>
    /// Obtenir la liste des cartes avec le modèle CarteDetailModel en asynchrone.
    /// </summary>
    /// <returns>Liste des cartes</returns>
    Task<List<CarteDetailModel>> ObtenirListeCarteDetailAsync();
}