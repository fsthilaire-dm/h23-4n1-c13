﻿using SuperCarte.Core.Models;

namespace SuperCarte.Core.Services;

/// <summary>
/// Interface qui contient les services du modèle Role
/// </summary>
public interface IRoleService
{
    /// <summary>
    /// Obtenir la liste des rôles dans un objet listeItem
    /// </summary>    
    /// <returns>Liste de rôles dans un ListItem</returns>
    List<ListeItem<int>> ObtenirListeItem();
}
