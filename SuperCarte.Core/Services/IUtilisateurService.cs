﻿using SuperCarte.Core.Models;

namespace SuperCarte.Core.Services;

/// <summary>
/// Interface qui contient les services du modèle Utilisateur
/// </summary>
public interface IUtilisateurService
{
    /// <summary>
    /// Obtenir un utilisateur en partir de sa clé primaire en asynchrone.
    /// </summary>
    /// <param name="utilisateurId">Clé primaire de l'utilisateur</param>
    /// <returns>L'utilisateur ou null si l'utilisateur n'est pas trouvé</returns>
    Task<UtilisateurModel?> ObtenirAsync(int utilisateurId);

    /// <summary>
    /// Obtenir un utilisateur en partir de sa clé primaire.
    /// </summary>
    /// <param name="utilisateurId">Clé primaire de l'utilisateur</param>
    /// <returns>L'utilisateur ou null si l'utilisateur n'est pas trouvé</returns>
    UtilisateurModel? Obtenir(int utilisateurId);

    /// <summary>
    /// Ajouter un utilisateur en asynchrone.
    /// </summary>
    /// <param name="utilisateurModel">Utilisateur à ajouter</param>        
    /// <param name="motPasse">Mot de passe</param>
    /// <returns>Vrai si ajouté, faux si non ajouté</returns>
    Task<bool> AjouterAsync(UtilisateurModel utilisateurModel, string motPasse);

    /// <summary>
    /// Modifier un utilisateur en asynchrone.
    /// </summary>
    /// <param name="utilisateurModel">Utilisateur à modifier</param>
    /// <returns>Vrai si ajouté, faux si non ajouté</returns>
    Task<bool> ModifierAsync(UtilisateurModel utilisateurModel);

    /// <summary>
    /// Authentitifer un utilisateur en asynchrone.
    /// </summary>
    /// <param name="nomUtilisateur">Nom utilisateur</param>
    /// <param name="motPasse">Mot de passe en clair</param>
    /// <returns>L'utilisateur authentifié ou null si la combinaison nom utilisateur et mot de passe est invalide</returns>
    Task<UtilisateurAuthentifieModel?> AuthentifierUtilisateurAsync(string nomUtilisateur, string motPasse);

    /// <summary>
    /// Vérifier l'autorisation d'un utilisateur à partir de rôles autorisés en asynchrone.
    /// </summary>
    /// <param name="utilisateurId">Utilisateur Id à autoriser</param>
    /// <param name="lstNomRole">Liste des noms des rôles autorisés</param>
    /// <returns>Vrai si l'utilisateur est autorisé, faux si non autorisé</returns>
    Task<bool> AutoriserUtilisateurParRolesAsync(int utilisateurId, List<string> lstNomRole);

    /// <summary>
    /// Vérifier l'autorisation d'un utilisateur à partir de rôles autorisés en asynchrone.
    /// </summary>
    /// <param name="utilisateurId">Utilisateur Id à autoriser</param>
    /// <param name="lstNomRole">Liste des noms des rôles autorisés</param>
    /// <returns>Vrai si l'utilisateur est autorisé, faux si non autorisé</returns>
    bool AutoriserUtilisateurParRoles(int utilisateurId, List<string> lstNomRole);

    /// <summary>
    /// Valider le modèle
    /// </summary>
    /// <param name="utilisateurModel">UtilisateurModel à valider</param>
    /// <returns>Résultat de validation</returns>
    Task<ValidationModel> ValiderAsync(UtilisateurModel utilisateurModel);

    /// <summary>
    /// Valider le modèle avec un mot de passe
    /// </summary>
    /// <param name="utilisateurModel">UtilisateurModel à valider</param>
    /// <param name="motPasse">Mot de passe à valider</param>
    /// <returns>Résultat de validation</returns>
    Task<ValidationModel> ValiderAsync(UtilisateurModel utilisateurModel, string motPasse);
}
