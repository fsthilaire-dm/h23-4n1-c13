﻿using SuperCarte.Core.Extensions;
using SuperCarte.Core.Models;
using SuperCarte.Core.Repositories;
using SuperCarte.Core.Validateurs;
using SuperCarte.EF.Data;

namespace SuperCarte.Core.Services;

/// <summary>
/// Classe qui contient les services du modèle UtilisateurCarte
/// </summary>
public class UtilisateurCarteService : IUtilisateurCarteService
{
    private readonly IUtilisateurCarteRepo _utilisateurCarteRepo;
    private readonly IValidateurPropriete<UtilisateurCarteModel> _utilisateurCarteValidateur;

    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="utilisateurCarteRepo">Repository UtilisateurCarte</param>
    public UtilisateurCarteService(IUtilisateurCarteRepo utilisateurCarteRepo,
        IValidateurPropriete<UtilisateurCarteModel> utilisateurCarteValidateur)
    {
        _utilisateurCarteRepo = utilisateurCarteRepo;
        _utilisateurCarteValidateur = utilisateurCarteValidateur;
    }

    public async Task<List<QuantiteCarteDetailModel>> ObtenirCartesUtilisateurAsync(int utilisateurId)
    {
        return await _utilisateurCarteRepo.ObtenirCartesUtilisateurAsync(utilisateurId);
    }

    public async Task<UtilisateurCarteModel?> ObtenirAsync(int utilisateurId, int carteId)
    {
        UtilisateurCarte? utilisateurCarte = await _utilisateurCarteRepo.ObtenirParCleAsync(utilisateurId, carteId);

        return utilisateurCarte?.VersUtilisateurCarteModel();
    }

    public UtilisateurCarteDetailModel? ObtenirDetail(int utilisateurId, int carteId)
    {
        return _utilisateurCarteRepo.ObtenirDetailParCle(utilisateurId, carteId);        
    }

    public UtilisateurCarteModel? Obtenir(int utilisateurId, int carteId)
    {
        UtilisateurCarte? utilisateurCarte = _utilisateurCarteRepo.ObtenirParCle(utilisateurId, carteId);

        return utilisateurCarte?.VersUtilisateurCarteModel();
    }

    public List<ListeItem<int>> ObtenirCartesDisponibles(int utilisateurId)
    {
        return _utilisateurCarteRepo.ObtenirCartesDisponibles(utilisateurId);
    }

    public async Task<bool> AjouterAsync(UtilisateurCarteModel utilisateurCarteModel)
    {        
        if ((await ValiderAsync(utilisateurCarteModel, true)).EstValide == true)
        {
            //Transformation de l'objet du modèle du domaine en objet du modèle de données
            UtilisateurCarte utilisateurCarte = utilisateurCarteModel.VersUtilisateurCarte();

            //Ajout dans repository avec enregistrement immédiat
            await _utilisateurCarteRepo.AjouterAsync(utilisateurCarte, true);

            utilisateurCarteModel.Copie(utilisateurCarte, false);

            return true;
        }
        else
        {
            return false;
        }
    }

    public async Task<bool> ModifierAsync(UtilisateurCarteModel utilisateurCarteModel)
    {        
        if ((await ValiderAsync(utilisateurCarteModel, false)).EstValide == true)
        {
            UtilisateurCarte? utilisateurCarte =
                await _utilisateurCarteRepo.ObtenirParCleAsync(utilisateurCarteModel.UtilisateurId,
                    utilisateurCarteModel.CarteId);

            if (utilisateurCarte != null)
            {
                //Assigner les valeurs dans le UtilisateurCarte
                utilisateurCarte.Copie(utilisateurCarteModel);

                await _utilisateurCarteRepo.EnregistrerAsync();

                //Assigne les valeurs de la base de données dans l'objet du modèle
                utilisateurCarteModel.Copie(utilisateurCarte, false);
            }
            else
            {
                throw new Exception("Impossible de modifier l'item UtilisateurCarte. Aucun UtilisateurCarte trouvé avec la clé primaire.");
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    public async Task<ValidationModel> ValiderAsync(UtilisateurCarteModel utilisateurCarteModel, bool validerPourAjout)
    {
        if (validerPourAjout == true)
        {
            return await _utilisateurCarteValidateur.ValiderAsync(utilisateurCarteModel);
        }
        else
        {
            return await _utilisateurCarteValidateur.ValiderAsync(utilisateurCarteModel,
                nameof(utilisateurCarteModel.Quantite));
        }
    }
}