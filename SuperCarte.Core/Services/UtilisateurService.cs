﻿using SuperCarte.Core.Extensions;
using SuperCarte.Core.Models;
using SuperCarte.Core.Repositories;
using SuperCarte.Core.Validateurs;
using SuperCarte.EF.Data;
using BC = BCrypt.Net.BCrypt; //La classe a le même nom qu'une partie du namespace. Cette nomenclature permet de renommer la classe.

namespace SuperCarte.Core.Services;

/// <summary>
/// Classe qui contient les services du modèle Utilisateur
/// </summary>
public class UtilisateurService : IUtilisateurService
{
    private readonly IUtilisateurRepo _utilisateurRepo;
    private readonly IUtilisateurValidateur _validateur;

    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="utilisateurRepo">Repository Utilisateur</param>
    /// <param name="validateur">Validateur</param>
    public UtilisateurService(IUtilisateurRepo utilisateurRepo, IUtilisateurValidateur validateur)
    {
        _utilisateurRepo = utilisateurRepo;
        _validateur = validateur;
    }

    public async Task<bool> AjouterAsync(UtilisateurModel utilisateurModel, string motPasse)
    {
        if ((await _validateur.ValiderAsync(utilisateurModel, motPasse)).EstValide == true)
        {
            //Transformation de l'objet du modèle du domaine en objet du modèle de données
            //Utilisateur utilisateur = utilisateurModel.VersUtilisateur();
            Utilisateur utilisateur = new Utilisateur()
            {
                UtilisateurId = utilisateurModel.UtilisateurId,
                Prenom = utilisateurModel.Prenom,
                Nom = utilisateurModel.Nom,
                NomUtilisateur = utilisateurModel.NomUtilisateur,
                RoleId = utilisateurModel.RoleId
            };

            //Le mot de passe n'est pas copié, il faut le convertir avec BCrypt
            utilisateur.MotPasseHash = BC.HashPassword(motPasse);

            //Ajout dans repository avec enregistrement immédiat
            await _utilisateurRepo.AjouterAsync(utilisateur, true);

            //Assigne les valeurs de la base de données dans l'objet du modèle
            utilisateurModel.Copie(utilisateur, true);

            return true;
        }
        else
        {
            return false;
        }
    }

    public async Task<bool> ModifierAsync(UtilisateurModel utilisateurModel)
    {
        //Il n'y a aucune référence au mot de passe.
        if ((await _validateur.ValiderAsync(utilisateurModel)).EstValide == true)
        {
            Utilisateur? utilisateur = await _utilisateurRepo.ObtenirParCleAsync(utilisateurModel.UtilisateurId);

            if (utilisateur != null)
            {
                //Assigner les valeurs dans l'utilisateur, sauf pour le mot de passe.
                utilisateur.Copie(utilisateurModel);

                await _utilisateurRepo.EnregistrerAsync();

                //Assigne les valeurs de la base de données dans l'objet du modèle
                utilisateurModel.Copie(utilisateur, false);

                return true;
            }
            else
            {
                throw new Exception("Impossible de modifier l'utilisateur. Aucun utilisateur trouvé avec la clé primaire.");
            }
        }
        else
        {
            return false;
        }
    }

    public async Task<UtilisateurModel?> ObtenirAsync(int utilisateurId)
    {
        Utilisateur? utilisateur = await _utilisateurRepo.ObtenirParCleAsync(utilisateurId);
        
        return utilisateur?.VersUtilisateurModel();
    }

    public UtilisateurModel? Obtenir(int utilisateurId)
    {
        Utilisateur? utilisateur = _utilisateurRepo.ObtenirParCle(utilisateurId);

        return utilisateur?.VersUtilisateurModel();
    }

    public async Task<UtilisateurAuthentifieModel?> AuthentifierUtilisateurAsync(string nomUtilisateur, string motPasse)
    {
        Utilisateur? utilisateur = await _utilisateurRepo.ObtenirParNomUtilisateurAsync(nomUtilisateur);

        if (utilisateur != null)
        {
            if (BC.Verify(motPasse, utilisateur.MotPasseHash) == true)
            {
                return new UtilisateurAuthentifieModel()
                {
                    UtilisateurId = utilisateur.UtilisateurId,
                    NomUtilisateur = utilisateur.NomUtilisateur,
                    Prenom = utilisateur.Prenom,
                    Nom = utilisateur.Nom
                };
            }
            else
            {
                return null;
            }
        }
        else
        {
            return null;
        }
    }

    public async Task<bool> AutoriserUtilisateurParRolesAsync(int utilisateurId, List<string> lstNomRole)
    {
        Role? role = await _utilisateurRepo.ObtenirRoleUtilisateurAsync(utilisateurId);

        if (role != null)
        {
            return lstNomRole.Contains(role.Nom);
        }
        else
        {
            return false;
        }
    }

    public bool AutoriserUtilisateurParRoles(int utilisateurId, List<string> lstNomRole)
    {
        Role? role = _utilisateurRepo.ObtenirRoleUtilisateur(utilisateurId);

        if (role != null)
        {
            return lstNomRole.Contains(role.Nom);
        }
        else
        {
            return false;
        }
    }

    public async Task<ValidationModel> ValiderAsync(UtilisateurModel utilisateurModel)
    {
        return await _validateur.ValiderAsync(utilisateurModel);
    }

    public async Task<ValidationModel> ValiderAsync(UtilisateurModel utilisateurModel, string motPasse)
    {
        return await _validateur.ValiderAsync(utilisateurModel, motPasse);
    }
}
