﻿using SuperCarte.Core.Models;

namespace SuperCarte.Core.Validateurs;

/// <summary>
/// Interface qui valide un objet UtilisateurModel avec son mot de passe.
/// </summary>
public interface IUtilisateurValidateur : IValidateur<UtilisateurModel>
{
    /// <summary>
    /// Valider un UtilisateurModel avec son mot de passe
    /// </summary>
    /// <param name="utilisateurModel">Modèle à valider</param>
    /// <param name="motPasse">Mot de passe</param>
    /// <returns>Résultat de la validation</returns>
    Task<ValidationModel> ValiderAsync(UtilisateurModel utilisateurModel, string motPasse);
}