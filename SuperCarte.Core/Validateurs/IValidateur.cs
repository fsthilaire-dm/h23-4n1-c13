﻿using SuperCarte.Core.Models;
namespace SuperCarte.Core.Validateurs;

/// <summary>
/// Interface qui valide un modèle du domaine
/// </summary>
/// <typeparam name="TModele">Type du modèle du domaine à valider</typeparam>
public interface IValidateur<TModele> where TModele : class
{
    /// <summary>
    /// Valider un objet du modèle du domaine
    /// </summary>
    /// <param name="modele">Modèle à valider</param>
    /// <returns>Résultat de la validation</returns>
    Task<ValidationModel> ValiderAsync(TModele modele);
}