﻿using SuperCarte.Core.Models;

namespace SuperCarte.Core.Validateurs;

/// <summary>
/// Interface qui valide un modèle du domaine avec la possibilité de spécifier des propriétés spécifiques
/// </summary>
/// <typeparam name="TModele">Type du modèle du domaine à valider</typeparam>
public interface IValidateurPropriete<TModele>  where TModele : class
{
    /// <summary>
    /// Valider un objet du modèle du domaine pour des propriétés spécifiques
    /// </summary>
    /// <param name="modele">Modèle à valider</param>
    /// <param name="proprietesAValider">Propriété</param>
    /// <returns>Résultat de la validation</returns>
    Task<ValidationModel> ValiderAsync(TModele modele, params string[] proprietesAValider);
}
