﻿using FluentValidation;
using FluentValidation.Results;
using SuperCarte.Core.Extensions;
using SuperCarte.Core.Models;
using SuperCarte.Core.Repositories;

namespace SuperCarte.Core.Validateurs;

/// <summary>
/// Classe qui valide le modèle UtilisateurCarteModel
/// </summary>
public class UtilisateurCarteValidateur : AbstractValidator<UtilisateurCarteModel>, IValidateurPropriete<UtilisateurCarteModel>
{
    private readonly IUtilisateurCarteRepo _utilisateurCarteRepo;
    private readonly IUtilisateurRepo _utilisateurRepo;
    private readonly ICarteRepo _carteRepo;

    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="utilisateurCarteRepo">Repository de UtilisateurCarte</param>
    /// <param name="utilisateurRepo">Repository de Utilisateur</param>
    /// <param name="carteRepo">Repository de Carte</param>
    public UtilisateurCarteValidateur(IUtilisateurCarteRepo utilisateurCarteRepo, 
        IUtilisateurRepo utilisateurRepo, ICarteRepo carteRepo)
    {
        _utilisateurCarteRepo = utilisateurCarteRepo;
        _utilisateurRepo = utilisateurRepo;
        _carteRepo = carteRepo;

        RuleFor(i => (int)i.Quantite).Cascade(CascadeMode.Stop)
            .InclusiveBetween(1, short.MaxValue).WithMessage($"La quantité doit être entre 1 et {short.MaxValue:N0}");

        RuleFor(i => i.UtilisateurId).Cascade(CascadeMode.Stop)
            .Must(ValiderUtilisateurIdExiste).WithMessage("L'utilisateur sélectionné n'est pas valide.")
            .Must((i, p) => ValiderDoublon(p, i.CarteId)).WithMessage("L'utilisateur existe déjà pour cette carte.");

        RuleFor(i => i.CarteId).Cascade(CascadeMode.Stop)
            .Must(ValiderCarteIdExiste).WithMessage("La carte sélectionnée n'est pas valide.")
            .Must((i, p) => ValiderDoublon(i.UtilisateurId, p)).WithMessage("La carte existe déjà pour cet utilisateur.");
    }


    public async Task<ValidationModel> ValiderAsync(UtilisateurCarteModel modele, params string[] proprietesAValider)
    {
        ValidationResult validationResult;

        if (proprietesAValider?.Length > 0)
        {
            //Il y a des propriétés à valider
            validationResult = await this.ValidateAsync(modele, o => o.IncludeProperties(proprietesAValider));            
        }
        else
        {
            //Il n'y a aucune propriété à valider
            validationResult = await base.ValidateAsync(modele);
        }

        return validationResult.VersValidationModel();
    }

    /// <summary>
    /// Valider la clé primaire de l'utilisateur si elle existe
    /// </summary>
    /// <param name="utilisateurId">Clé primaire de l'utilisateur</param>
    /// <returns>Vrai si valide, faux si non valide</returns>
    private bool ValiderUtilisateurIdExiste(int utilisateurId)
    {
        return _utilisateurRepo.ObtenirParCle(utilisateurId) != null;
    }

    /// <summary>
    /// Valider la clé primaire de la carte si elle existe
    /// </summary>
    /// <param name="utilisateurId">Clé primaire de la carte</param>
    /// <returns>Vrai si valide, faux si non valide</returns>
    private bool ValiderCarteIdExiste(int carteId)
    {
        return _carteRepo.ObtenirParCle(carteId) != null;
    }

    /// <summary>
    /// Valider si la combinaison CarteId et UtilisateurId n'est pas déjà utilisée
    /// </summary>    
    /// <param name="utilisateurId">Clé primaire de l'utilisateur</param>
    /// <param name="carteId">Clé primaire de la carte</param>
    /// <returns>Vrai si valide, faux si non valide</returns>
    private bool ValiderDoublon(int utilisateurId, int carteId)
    {
        return _utilisateurCarteRepo.ObtenirParCle(utilisateurId, carteId) == null;
    }
}
