﻿using FluentValidation;
using FluentValidation.Results;
using SuperCarte.Core.Models;
using SuperCarte.Core.Extensions;

namespace SuperCarte.Core.Validateurs;

/// <summary>
/// Classe qui valide le modèle UtilisateurValidateur
/// </summary>
public class UtilisateurValidateur : AbstractValidator<UtilisateurModel>, IUtilisateurValidateur
{
    /// <summary>
    /// Constructeur
    /// </summary>
    public UtilisateurValidateur()
    {
        //Les règles de validations        
        RuleFor(i => i.Nom).Cascade(CascadeMode.Stop)
            .NotEmpty().WithMessage("Le nom est obligatoire.")
            .NotNull().WithMessage("Le nom est obligatoire.")
            .MaximumLength(35).WithMessage("Le nom doit avoir 50 caractères au maximum.");
    }

    public async Task<ValidationModel> ValiderAsync(UtilisateurModel utilisateurModel, string motPasse)
    {
        ValidationResult validationResult = await base.ValidateAsync(utilisateurModel);

        ValidationModel validationModel = validationResult.VersValidationModel();

        if (ValiderMotPasse(motPasse) == false)
        {
            validationModel.AssignerErreur("MotPasse", "La complexité du mot de passe n'est pas valide.");
        }

        return validationModel;
    }

    public async Task<ValidationModel> ValiderAsync(UtilisateurModel modele)
    {
        ValidationResult validationResult = await base.ValidateAsync(modele);

        return validationResult.VersValidationModel();
    }

    /// <summary>
    /// Validation du mot de passe
    /// </summary>
    /// <param name="motPasse">Mot de passe</param>
    /// <returns>Vrai si valide, faux si non valide</returns>
    private bool ValiderMotPasse(string motPasse)
    {
        return motPasse.Length >= 8;
    }
}