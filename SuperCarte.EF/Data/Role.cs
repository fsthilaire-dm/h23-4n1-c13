﻿namespace SuperCarte.EF.Data;

public class Role
{
    public int RoleId { get; set; }

    public string Nom { get; set; } = null!;

    public ICollection<Utilisateur> UtilisateurListe { get; set; } = new List<Utilisateur>();
}