﻿using CommunityToolkit.Mvvm.ComponentModel;
using Microsoft.Extensions.DependencyInjection;

namespace SuperCarte.WPF.Aides;

/// <summary>
/// Classe qui contient la mécanique de navigation de l'application
/// </summary>
public class Navigateur : ObservableObject, INavigateur
{
    private readonly IServiceProvider _serviceProvider;
    private BaseVM _vmActif;

    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="serviceProvider">Service Provider</param>
    public Navigateur(IServiceProvider serviceProvider)
	{
        _serviceProvider = serviceProvider;
    }    

    public void Naviguer<TViewModel>() where TViewModel : BaseVM
    {
        if (VMActif is not TViewModel)
        {
            VMActif = _serviceProvider.GetRequiredService<TViewModel>();
        }
    }

    public void Naviguer<TViewModel, TParameter>(TParameter parametre) where TViewModel : BaseParametreVM<TParameter>
    {
        if (VMActif is not TViewModel)
        {
            BaseParametreVM<TParameter> baseParametreVM = _serviceProvider.GetRequiredService<TViewModel>();

            baseParametreVM.AssignerParametre(parametre);

            VMActif = baseParametreVM;
        }
        else
        {
            (VMActif as BaseParametreVM<TParameter>).AssignerParametre(parametre);
        }
    }
    public BaseVM VMActif
    {
        get 
        { 
            return _vmActif; 
        }
        private set
        {
            SetProperty(ref _vmActif, value);
        }
    }
}
