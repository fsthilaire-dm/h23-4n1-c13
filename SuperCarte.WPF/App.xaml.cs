﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SuperCarte.Core.Services.AirTable;
using SuperCarte.WPF.Extensions.ServiceCollections;
using System.Globalization;
using System.Windows;

namespace SuperCarte.WPF;
/// <summary>
/// Interaction logic for App.xaml
/// </summary>
public partial class App : Application
{
    private IHost? _host;

	public App()
	{
        //Modification de la langue dans l'extension et du thread principal
        CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("fr-CA");
        WPFLocalizeExtension.Engine.LocalizeDictionary.Instance.SetCurrentThreadCulture = true;
        WPFLocalizeExtension.Engine.LocalizeDictionary.Instance.Culture = CultureInfo.DefaultThreadCurrentCulture;
        
        var builder = Host.CreateDefaultBuilder();

        //Enregistrement des services
        builder.ConfigureServices((context, services) =>
        {
            services.AddLocalization();
            services.AddSingleton<MainWindow>(); //Fenêtre principale

            //Enregistrement du contexte    
            services.AddDbContext<SuperCarteContext>(options => options.UseSqlServer(context.Configuration.GetConnectionString("DefaultConnection")));

            //Enregistrement des classes d'assistance
            services.AddSingleton<INavigateur, Navigateur>();
            services.AddSingleton<IAuthentificateur, Authentificateur>();
            services.AddSingleton<INotification, Notification>();

            //Appel des méthodes d'extension                        
            services.EnregistrerRepositories();
            services.EnregistrerServices();
            services.EnregistrerValidateurs();
            services.EnregistrerViewModels();
        });

        _host = builder.Build();
    }

    /// <summary>
    /// Démarrage de l'application
    /// </summary>
    /// <param name="e"></param>
    protected override async void OnStartup(StartupEventArgs e)
    {
        await _host!.StartAsync();

        //var s = _host.Services.GetRequiredService<IExportAirTableService>();
        //var r = await s.ExporterQuantiteCarteDetailModelAsync(2);

        var fenetreInitiale = _host.Services.GetRequiredService<MainWindow>();
        fenetreInitiale.Show(); //Affiche la fenêtre initiale
        base.OnStartup(e);
    }

    /// <summary>
    /// Fermeture de l'application
    /// </summary>
    /// <param name="e"></param>
    protected override async void OnExit(ExitEventArgs e)
    {
        await _host!.StopAsync();
        base.OnExit(e);
    }
}
