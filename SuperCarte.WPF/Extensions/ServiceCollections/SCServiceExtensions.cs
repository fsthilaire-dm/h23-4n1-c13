﻿using AirTableApiClient;
using Microsoft.Extensions.DependencyInjection;
using SuperCarte.Core.Services.AirTable;

namespace SuperCarte.WPF.Extensions.ServiceCollections;

/// <summary>
/// Classe d'extension qui permet d'enregistrer les classes de la catégorie Service
/// </summary>
public static class SCServiceExtensions
{
    /// <summary>
    /// Méthode qui permet d'enregistrer les services de l'application
    /// </summary>
    /// <param name="services">La collection de services</param>
    public static void EnregistrerServices(this IServiceCollection services)
    {
        services.AddScoped<ICategorieService, CategorieService>();
        services.AddScoped<ICarteService, CarteService>();
        services.AddScoped<IRoleService, RoleService>();
        services.AddScoped<IUtilisateurService, UtilisateurService>();
        services.AddScoped<IUtilisateurCarteService, UtilisateurCarteService>();
        services.AddScoped<IExportAirTableService, ExportAirTableService>();
        services.AddScoped<IAirTableClient, AirTableClient>();
    }
}
