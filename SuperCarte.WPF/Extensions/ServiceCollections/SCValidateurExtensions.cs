﻿using Microsoft.Extensions.DependencyInjection;
using SuperCarte.Core.Validateurs;

namespace SuperCarte.WPF.Extensions.ServiceCollections;

/// <summary>
/// Classe d'extension qui permet d'enregistrer les classes de la catégorie Validateur
/// </summary>
public static class SCValidateurExtensions
{
    /// <summary>
    /// Méthode qui permet d'enregistrer les validateurs de l'application
    /// </summary>
    /// <param name="services">La collection de services</param>
    public static void EnregistrerValidateurs(this IServiceCollection services)
    {
        services.AddScoped<IValidateur<CategorieModel>, CategorieValidateur>();
        services.AddScoped<IValidateurPropriete<UtilisateurCarteModel>, UtilisateurCarteValidateur>();
        services.AddScoped<IUtilisateurValidateur, UtilisateurValidateur>();
    }
}