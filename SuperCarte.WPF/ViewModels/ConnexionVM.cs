﻿using CommunityToolkit.Mvvm.Input;

namespace SuperCarte.WPF.ViewModels;

/// <summary>
/// ViewModel de la vue Connexion
/// </summary>
public class ConnexionVM : BaseVM
{
    #region Dépendances
    private readonly INavigateur _navigateur;
    private readonly IAuthentificateur _authentificateur;
    #endregion

    #region Attributs des propriétés        
    private string _nomUtilisateur;
    private string _motPasse;
    private bool _estEnTravail = false;
    private bool _champsModifiables = true;
    #endregion

    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="navigateur">Classe d'assistance pour la navigation</param>
    /// <param name="authentificateur">Classe d'assistance pour l'authentification</param>
    public ConnexionVM(INavigateur navigateur, IAuthentificateur authentificateur)
    {
        _navigateur = navigateur;
        _authentificateur = authentificateur;

        AuthentifierCommande = new AsyncRelayCommand(AuthentifierAsync, PeutAuthentifier);
    }

    #region Méthodes des commandes
    private async Task AuthentifierAsync()
    {
        ChampsModifiables = false;
        EstEnTravail = true;
        EffacerErreurs();

        bool authentifier = await _authentificateur.AuthentifierUtilisateurAsync(NomUtilisateur, MotPasse);

        if (authentifier == true)
        {
            _navigateur.Naviguer<ListeMesCartesVM>();
        }
        else
        {
            AjouterErreur(nameof(MotPasse), "La combinaison du nom d'utilisateur et du mot de passe n'est pas valide.");
        }

        EstEnTravail = false;
        ChampsModifiables = true;
    }

    private bool PeutAuthentifier()
    {
        return (!string.IsNullOrWhiteSpace(NomUtilisateur) && !string.IsNullOrWhiteSpace(MotPasse));
    }
    #endregion

    #region Commandes
    public IAsyncRelayCommand AuthentifierCommande { get; private set; }
    #endregion

    #region Propriétés liées
    public bool EstEnTravail
    {
        get
        {
            return _estEnTravail;
        }
        set
        {
            SetProperty(ref _estEnTravail, value);
        }
    }

    public bool ChampsModifiables
    {
        get
        {
            return _champsModifiables;
        }
        set
        {
            SetProperty(ref _champsModifiables, value);
        }
    }

    public string NomUtilisateur
    {
        get
        {
            return _nomUtilisateur;
        }
        set
        {
            if (SetProperty(ref _nomUtilisateur, value))
            {
                AuthentifierCommande.NotifyCanExecuteChanged();
            }
        }
    }

    public string MotPasse
    {
        get
        {
            return _motPasse;
        }
        set
        {
            if (SetProperty(ref _motPasse, value))
            {
                AuthentifierCommande.NotifyCanExecuteChanged();
            }
        }
    }
    #endregion
}
