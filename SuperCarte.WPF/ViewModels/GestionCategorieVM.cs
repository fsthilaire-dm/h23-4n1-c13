﻿using CommunityToolkit.Mvvm.Input;
using SuperCarte.Core.Models;
using SuperCarte.WPF.Views;

namespace SuperCarte.WPF.ViewModels;

/// <summary>
/// ViewModel de la vue Gestion Categorie
/// </summary>
public class GestionCategorieVM : BaseParametreVM<int>
{
    #region Dépendances
    private readonly ICategorieService _categorieService;
    #endregion

    #region Attributs des propriétés
    private int _categorieId;
    private string _nom;
    private string? _description;
    private bool _estEnTravail = false;
    private bool _champsModifiables = true;
    #endregion

    public GestionCategorieVM(ICategorieService categorieService)
	{
        _categorieService = categorieService;

        EnregistrerCommande = new AsyncRelayCommand(EnregistrerAsync, () => !EstEnTravail);
        ObtenirCommande = new AsyncRelayCommand(ObtenirAsync, () => !EstEnTravail);
        NouveauCommande = new RelayCommand(Nouveau, () => !EstEnTravail);
    }

    #region Méthodes des commandes
    /// <summary>
    /// Enregistrer la catégorie
    /// </summary>    
    private async Task EnregistrerAsync()
    {
        EffacerErreurs();

        ChampsModifiables = false;
        EstEnTravail = true;
        bool estEnregistre;

        CategorieModel categorieModel = VersModele();
        
        ValidationModel validationModel = await _categorieService.ValiderAsync(categorieModel);

        if (validationModel.EstValide == true)
        {
            if (categorieModel.CategorieId == 0)
            {
                //La clé primaire est zéro, donc c'est une nouvelle catégorie
                estEnregistre = await _categorieService.AjouterAsync(categorieModel);
            }
            else
            {
                //La clé primaire n'est pas zéro, donc c'est une catégorie existante
                estEnregistre = await _categorieService.ModifierAsync(categorieModel);
            }

            if (estEnregistre == true)
            {
                VersVM(categorieModel);
            }
            else
            {
                //Envoyer un message d'erreur à la vue
                throw new Exception("Erreur. Impossible d'enregistrer");
            }
        }
        else
        {
            AssignerValidation(validationModel);
        }

        EstEnTravail = false;
        ChampsModifiables = true;
    }

    /// <summary>
    /// Obtenir la catégorie
    /// </summary>
    /// <returns></returns>
    private async Task ObtenirAsync()
    {
        EstEnTravail = true;

        CategorieModel? categorieModel = await _categorieService.ObtenirAsync(CategorieId);

        VersVM(categorieModel);

        EstEnTravail = false;
    }

    /// <summary>
    /// Mettre le ViewModel en mode ajouter
    /// </summary>
    private void Nouveau()
    {
        CategorieId = 0;
        Nom = string.Empty;
        Description = null;
    }
    #endregion

    #region Commandes
    public IAsyncRelayCommand EnregistrerCommande { get; private set; }

    public IAsyncRelayCommand ObtenirCommande { get; private set; }

    public IRelayCommand NouveauCommande { get; private set; }
    #endregion   

    #region Méthodes d'assignation
    /// <summary>
    /// Assigner les propriétés liées du ViewModel vers les propriétés du modèle
    /// </summary>
    /// <returns>Objet du modèle</returns>
    private CategorieModel VersModele()
    {
        return new CategorieModel
        {
            CategorieId = this.CategorieId,
            Nom = this.Nom,
            Description = this.Description
        };
    }

    /// <summary>
    /// Assigner les propriétés du modèle vers les propriétés liées du ViewModel
    /// </summary>
    /// <param name="categorieModel">Modèle</param>
    private void VersVM(CategorieModel? categorieModel)
    {
        if (categorieModel != null)
        { 
            CategorieId = categorieModel.CategorieId;
            Nom = categorieModel.Nom;
            Description = categorieModel.Description;
        }
        else
        {
            CategorieId = 0;
            Nom = string.Empty;
            Description = null;
        }
    }

    public override void AssignerParametre(int parametre)
    {
        CategorieId = parametre;

        CategorieModel? categorieModel = _categorieService.Obtenir(CategorieId);

        VersVM(categorieModel);
    }
    #endregion

    #region Propriétés liées
    public bool EstEnTravail
    {
        get
        {
            return _estEnTravail;
        }
        set
        {
            if (SetProperty(ref _estEnTravail, value))
            {
                ObtenirCommande.NotifyCanExecuteChanged();
                EnregistrerCommande.NotifyCanExecuteChanged();
                NouveauCommande.NotifyCanExecuteChanged();
            }
        }
    }

    public bool ChampsModifiables
    {
        get
        {
            return _champsModifiables;
        }
        set
        {
            SetProperty(ref _champsModifiables, value);
        }
    }

    public int CategorieId
    {
        get 
        { 
            return _categorieId;
        }
        private set
        {
            SetProperty(ref _categorieId, value);
        }
    }

    public string Nom
    {
        get
        {
            return _nom;
        }
        set
        {
            SetProperty(ref _nom, value);
        }
    }

    public string? Description
    {
        get
        {
            return _description;
        }
        set
        {
            //Permet de remplacer une chaine vide par null
            SetProperty(ref _description, string.IsNullOrWhiteSpace(value) ? null : value );
        }
    }
    #endregion
}
