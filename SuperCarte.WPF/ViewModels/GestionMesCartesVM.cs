﻿using CommunityToolkit.Mvvm.Input;
using SuperCarte.Core.Models;
using SuperCarte.EF.Data;
using System.Linq;

namespace SuperCarte.WPF.ViewModels;

/// <summary>
/// ViewModel de la vue Gestion MesCartes
/// </summary>
public class GestionMesCartesVM : BaseParametreVM<int>
{
    #region Attributs
    private readonly string[] _rolesAutorises = { "Administrateur", "Utilisateur" };
    private int _carteIdCle;
    #endregion

    #region Dépendances
    private readonly IAuthentificateur _authentificateur;
    private readonly INotification _notification;
    private readonly IUtilisateurCarteService _utilisateurCarteService;
    #endregion

    #region Attributs des propriétés
    private int _carteId;
    private short _quantite;
    private List<ListeItem<int>> _lstCartesDisponibles = new List<ListeItem<int>>();
    private bool _estEnTravail = false;
    private bool _champsModifiables = true;
    #endregion

    /// <summary>
    /// Constructeur
    /// </summary>    
    /// <param name="authentificateur">La classe d'assistance d'authentification</param>
    /// <param name="notification">La classe d'assistance pour la notification</param>
    /// <param name="utilisateurCarteService">Le service du modèle UtilisateurCarte</param>
    public GestionMesCartesVM(IAuthentificateur authentificateur, INotification notification,
        IUtilisateurCarteService utilisateurCarteService)
    {
        _authentificateur = authentificateur;
        _notification = notification;        

        if (_authentificateur.EstAutorise(_rolesAutorises))
        {
            _utilisateurCarteService = utilisateurCarteService;

            EnregistrerCommande = new AsyncRelayCommand(EnregistrerAsync, () => !EstEnTravail);
            ObtenirCommande = new AsyncRelayCommand(ObtenirAsync, () => !EstEnTravail);
            NouveauCommande = new RelayCommand(Nouveau, () => !EstEnTravail);
        }
        else
        {
            _notification.MessageErreur("Accès non autorisé.", "Vous n'avez pas accès à cette vue.");
        }
    }

    #region Méthodes d'assignation
    /// <summary>
    /// Assigner les propriétés liées du ViewModel vers les propriétés du modèle
    /// </summary>
    /// <returns>Objet du modèle</returns>
    private UtilisateurCarteModel VersModele()
    {
        return new UtilisateurCarteModel
        {
            UtilisateurId = _authentificateur.UtilisateurAuthentifie!.UtilisateurId,
            //Si la clé est 0, il faut prendre celle sélectionnée par l'utilisateur, sinon il faut prendre celle reçue en paramètre
            CarteId = (this.CarteIdCle == 0 ? this.CarteId : this.CarteIdCle),
            Quantite = this.Quantite
        };
    }

    /// <summary>
    /// Assigner les propriétés du modèle vers les propriétés liées du ViewModel
    /// </summary>
    /// <param name="categorieModel">Modèle</param>
    private void VersVM(UtilisateurCarteModel? utilisateurCarteModel)
    {
        if (utilisateurCarteModel != null)
        {
            CarteIdCle = utilisateurCarteModel.CarteId;
            CarteId = utilisateurCarteModel.CarteId;
            Quantite = utilisateurCarteModel.Quantite;
        }
        else
        {
            CarteIdCle = 0;
            CarteId = 0;
            Quantite = 0;
        }
    }    

    public override void AssignerParametre(int parametre)
    {
        if (_authentificateur.EstAutorise(_rolesAutorises))
        {
            //Le paramètre est CarteIdCle
            CarteIdCle = parametre;                        

            if (CarteIdCle == 0)
            {
                ObtenirListeCartesDisponibles();
                VersVM(null);
            }
            else
            {
                UtilisateurCarteDetailModel? utilisateurCarteDetailModel =
                    _utilisateurCarteService.ObtenirDetail(_authentificateur.UtilisateurAuthentifie!.UtilisateurId,
                                                           CarteIdCle);

                ListeCartesDisponibles.Clear();
                ListeCartesDisponibles.Add(new ListeItem<int>()
                {
                    Valeur = utilisateurCarteDetailModel.CarteId,
                    Texte = $"{utilisateurCarteDetailModel.CarteNom} ({utilisateurCarteDetailModel.CategorieNom}) ({utilisateurCarteDetailModel.EnsembleNom})"
                });

                VersVM(utilisateurCarteDetailModel);
            }                        
        }
    }
    #endregion

    #region Méthodes des commandes
    /// <summary>
    /// Enregistrer la catégorie
    /// </summary>    
    private async Task EnregistrerAsync()
    {
        EffacerErreurs();

        ChampsModifiables = false;
        EstEnTravail = true;

        if (_authentificateur.EstAutorise(_rolesAutorises))
        {
            bool estEnregistre = true;

            UtilisateurCarteModel utilisateurCarteModel = VersModele();            
            ValidationModel validationModel = await _utilisateurCarteService.ValiderAsync(utilisateurCarteModel, CarteIdCle == 0);

            if (validationModel.EstValide == true)
            {
                if (CarteIdCle == 0)
                {
                    //C'est un nouveau
                    estEnregistre = await _utilisateurCarteService.AjouterAsync(utilisateurCarteModel);
                }
                else
                {
                    //C'est une modification
                    estEnregistre = await _utilisateurCarteService.ModifierAsync(utilisateurCarteModel);
                }

                if (estEnregistre == true)
                {
                    VersVM(utilisateurCarteModel);
                }
                else
                {
                    //Envoyer un message d'erreur à la vue
                    throw new Exception("Erreur. Impossible d'enregistrer");
                }
            }
            else
            {
                AssignerValidation(validationModel);
            }
        }
        else
        {
            _notification.MessageErreur("Accès non autorisé.", "Vous n'avez pas accès à cette fonctionnalité.");
        }

        EstEnTravail = false;
        ChampsModifiables = true;
    }

    /// <summary>
    /// Obtenir l'utilisateur
    /// </summary>
    /// <returns></returns>
    private async Task ObtenirAsync()
    {
        EstEnTravail = true;

        if (_authentificateur.EstAutorise(_rolesAutorises))
        {
            UtilisateurCarteModel? utilisateurCarteModel = await
                _utilisateurCarteService.ObtenirAsync(_authentificateur.UtilisateurAuthentifie!.UtilisateurId,
                                                      CarteIdCle);

            VersVM(utilisateurCarteModel);
        }
        else
        {
            _notification.MessageErreur("Accès non autorisé.", "Vous n'avez pas accès à cette fonctionnalité.");
        }

        EstEnTravail = false;
    }

    /// <summary>
    /// Mettre le ViewModel en mode ajouter
    /// </summary>
    private void Nouveau()
    {
        ObtenirListeCartesDisponibles();

        CarteIdCle = 0;
        CarteId = 0;
        Quantite = 0;
    }

    /// <summary>
    /// Obtenir la liste des cartes disponibles pour l'utilisateur
    /// </summary>
    private void ObtenirListeCartesDisponibles()
    {
        ListeCartesDisponibles.Clear();
        ListeCartesDisponibles.Insert(0, new ListeItem<int>() { Valeur = 0, Texte = "Veuillez sélectionner une carte..." });
        ListeCartesDisponibles.AddRange(_utilisateurCarteService.ObtenirCartesDisponibles(_authentificateur.UtilisateurAuthentifie!.UtilisateurId));
    }
    #endregion

    #region Commandes
    public IAsyncRelayCommand EnregistrerCommande { get; private set; }
    public IAsyncRelayCommand ObtenirCommande { get; private set; }

    public IRelayCommand NouveauCommande { get; private set; }
    #endregion   


    #region Propriétés liées
    public bool EstEnTravail
    {
        get
        {
            return _estEnTravail;
        }
        set
        {
            if (SetProperty(ref _estEnTravail, value))
            {
                ObtenirCommande.NotifyCanExecuteChanged();
                EnregistrerCommande.NotifyCanExecuteChanged();
                NouveauCommande.NotifyCanExecuteChanged();
            }
        }
    }

    public bool ChampsModifiables
    {
        get
        {
            return _champsModifiables;
        }
        set
        {
            SetProperty(ref _champsModifiables, value);
        }
    }

    public List<ListeItem<int>> ListeCartesDisponibles
    {
        get
        {
            return _lstCartesDisponibles;
        }
        private set
        {
            SetProperty(ref _lstCartesDisponibles, value);
        }
    }

    public int CarteId
    {
        get
        {
            return _carteId;
        }
        set
        {
            SetProperty(ref _carteId, value);
        }
    }

    public short Quantite
    {
        get
        {
            return _quantite;
        }
        set
        {
            SetProperty(ref _quantite, value);
        }
    }

    public bool CarteIdModifiable
    {
        get
        {
            return CarteIdCle == 0;
        }
    }

    private int CarteIdCle
    {
        get
        {
            return _carteIdCle;
        }
        set
        {
            if (_carteIdCle != value)
            {
                _carteIdCle = value;
                OnPropertyChanged(nameof(CarteIdModifiable));
            }
        }
    }
    #endregion
}