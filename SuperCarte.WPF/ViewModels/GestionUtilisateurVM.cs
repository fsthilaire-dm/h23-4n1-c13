﻿using CommunityToolkit.Mvvm.Input;
using SuperCarte.EF.Data;

namespace SuperCarte.WPF.ViewModels;

public class GestionUtilisateurVM : BaseParametreVM<int>
{
    #region Dépendances
    private readonly IUtilisateurService _utilisateurService;
    private readonly IRoleService _roleService;
    #endregion

    #region Attributs des propriétés
    private int _utilisateurId;
    private string _prenom;
    private string _nom;
    private string _nomUtilisateur;
    private string? _motPasse;
    private int _roleId;
    private List<ListeItem<int>> _lstRole;
    private bool _estEnTravail = false;
    private bool _champsModifiables = true;
    #endregion

    public GestionUtilisateurVM(IUtilisateurService utilisateurService, IRoleService roleService)
    {
        _utilisateurService = utilisateurService;
        _roleService = roleService;

        ListeRoles = _roleService.ObtenirListeItem();
        ListeRoles.Insert(0, new ListeItem<int>() { Valeur = 0, Texte = "Veuillez sélectionner un rôle..."});

        EnregistrerCommande = new AsyncRelayCommand(EnregistrerAsync, () => !EstEnTravail);
        ObtenirCommande = new AsyncRelayCommand(ObtenirAsync, () => !EstEnTravail);
        NouveauCommande = new RelayCommand(Nouveau, () => !EstEnTravail);
    }

    #region Méthodes d'assignation
    /// <summary>
    /// Assigner les propriétés liées du ViewModel vers les propriétés du modèle
    /// </summary>
    /// <returns>Objet du modèle</returns>
    private UtilisateurModel VersModele()
    {
        return new UtilisateurModel
        {
            UtilisateurId = this.UtilisateurId,
            Prenom = this.Prenom,
            Nom = this.Nom,
            NomUtilisateur = this.NomUtilisateur,
            RoleId= this.RoleId
        };
    }

    /// <summary>
    /// Assigner les propriétés du modèle vers les propriétés liées du ViewModel
    /// </summary>
    /// <param name="categorieModel">Modèle</param>
    private void VersVM(UtilisateurModel? utilisateurModel)
    {
        if (utilisateurModel != null)
        {
            UtilisateurId = utilisateurModel.UtilisateurId;
            Prenom = utilisateurModel.Prenom;
            Nom = utilisateurModel.Nom;
            NomUtilisateur = utilisateurModel.NomUtilisateur;
            RoleId = utilisateurModel.RoleId;            
        }
        else
        {
            UtilisateurId = 0;
            Prenom = string.Empty;
            Nom = string.Empty;
            NomUtilisateur = string.Empty;
            RoleId = 0;
        }
        
        MotPasse = string.Empty;
    }

    public override void AssignerParametre(int parametre)
    {
        UtilisateurId = parametre;

        UtilisateurModel? utilisateurModel = _utilisateurService.Obtenir(UtilisateurId);

        VersVM(utilisateurModel);
    }
    #endregion

    #region Méthodes des commandes
    /// <summary>
    /// Enregistrer la catégorie
    /// </summary>    
    private async Task EnregistrerAsync()
    {
        EffacerErreurs();

        ChampsModifiables = false;
        EstEnTravail = true;
        bool estEnregistre;

        UtilisateurModel utilisateurModel = VersModele();

        ValidationModel validationModel;

        if (utilisateurModel.UtilisateurId == 0)
        {
            validationModel = await _utilisateurService.ValiderAsync(utilisateurModel, MotPasse!);
        }
        else
        {
            validationModel = await _utilisateurService.ValiderAsync(utilisateurModel);
        }

        if (validationModel.EstValide == true)
        {

            if (utilisateurModel.UtilisateurId == 0)
            {
                //La clé primaire est zéro, donc c'est un nouvel utilisateur
                estEnregistre = await _utilisateurService.AjouterAsync(utilisateurModel, MotPasse!);
            }
            else
            {
                //La clé primaire n'est pas zéro, donc c'est un utilisateur existant
                estEnregistre = await _utilisateurService.ModifierAsync(utilisateurModel);
            }

            if (estEnregistre == true)
            {
                VersVM(utilisateurModel);
            }
            else
            {
                //Envoyer un message d'erreur à la vue
                throw new Exception("Erreur. Impossible d'enregistrer");
            }
        }
        else
        {
            AssignerValidation(validationModel);
        }

        EstEnTravail = false;
        ChampsModifiables = true;
    }

    /// <summary>
    /// Obtenir l'utilisateur
    /// </summary>
    /// <returns></returns>
    private async Task ObtenirAsync()
    {
        EstEnTravail = true;

        UtilisateurModel? utilisateurModel = await _utilisateurService.ObtenirAsync(UtilisateurId);

        VersVM(utilisateurModel);

        EstEnTravail = false;
    }

    /// <summary>
    /// Mettre le ViewModel en mode ajouter
    /// </summary>
    private void Nouveau()
    {
        UtilisateurId = 0;
        Prenom = string.Empty;
        Nom = string.Empty;
        NomUtilisateur = string.Empty;
        MotPasse = string.Empty;
        RoleId = 0;
    }
    #endregion

    #region Commandes
    public IAsyncRelayCommand EnregistrerCommande { get; private set; }

    public IAsyncRelayCommand ObtenirCommande { get; private set; }

    public IRelayCommand NouveauCommande { get; private set; }
    #endregion   

    #region Propriétés liées
    public bool EstEnTravail
    {
        get
        {
            return _estEnTravail;
        }
        set
        {
            if (SetProperty(ref _estEnTravail, value))
            {
                ObtenirCommande.NotifyCanExecuteChanged();
                EnregistrerCommande.NotifyCanExecuteChanged();
                NouveauCommande.NotifyCanExecuteChanged();
            }
        }
    }

    public bool ChampsModifiables
    {
        get
        {
            return _champsModifiables;
        }
        set
        {
            SetProperty(ref _champsModifiables, value);
        }
    }

    public bool MotPasseModifiable
    {
        get
        {
            return UtilisateurId == 0;
        }
    }

    public int UtilisateurId 
    { 
        get
        {
            return _utilisateurId;
        }
        set
        {
            if(SetProperty(ref _utilisateurId, value))
            {
                OnPropertyChanged(nameof(MotPasseModifiable));
            }
        }
    }

    public string Prenom
    {
        get
        {
            return _prenom;
        }
        set
        {
            SetProperty(ref _prenom, value);
        }
    }

    public string Nom
    {
        get
        {
            return _nom;
        }
        set
        {
            SetProperty(ref _nom, value);
        }
    }

    public string NomUtilisateur
    {
        get
        {
            return _nomUtilisateur;
        }
        set
        {
            SetProperty(ref _nomUtilisateur, value);
        }
    }

    public string? MotPasse
    {
        get
        {
            return _motPasse;
        }
        set
        {
            SetProperty(ref _motPasse, value);
        }
    }

    public int RoleId
    {
        get
        {
            return _roleId;
        }
        set
        {
            SetProperty(ref _roleId, value);
        }
    }

    public List<ListeItem<int>> ListeRoles
    {
        get
        {
            return _lstRole;
        }

        private set
        {
            SetProperty(ref _lstRole, value);
        }
    }
    #endregion
}
