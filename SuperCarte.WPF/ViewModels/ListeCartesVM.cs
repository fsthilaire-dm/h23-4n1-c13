﻿using CommunityToolkit.Mvvm.Input;

namespace SuperCarte.WPF.ViewModels
{
    /// <summary>
    /// ViewModel de la vue ListeCartes
    /// </summary>
    public class ListeCartesVM : BaseVM
    {
        #region Dépendances
        private readonly ICarteService _carteService;
        #endregion

        #region Attributs des propriétés
        private List<CarteDetailModel> _lstCartes;
        private CarteDetailModel? _carteSelection;
        private bool _estEnTravail = false; //À généraliser pour le TP 3
        #endregion

        public ListeCartesVM(ICarteService carteService)
        {
            _carteService = carteService;

            ObtenirListeCommande = new AsyncRelayCommand(ObtenirListeAsync);
        }

        #region Méthodes des commandes
        /// <summary>
        /// Obtenir la liste de catégories du service
        /// </summary>    
        private async Task ObtenirListeAsync()
        {
            EstEnTravail = true;

            ListeCartes = await _carteService.ObtenirListeCarteDetailAsync();

            EstEnTravail = false;
        }
        #endregion

        #region Commandes
        public IAsyncRelayCommand ObtenirListeCommande { get; private set; }
        #endregion

        #region Propriétés liées
        public bool EstEnTravail
        {
            get
            {
                return _estEnTravail;
            }
            set
            {
                SetProperty(ref _estEnTravail, value);
            }
        }

        public List<CarteDetailModel> ListeCartes
        {
            get
            {
                return _lstCartes;
            }
            set
            {
                SetProperty(ref _lstCartes, value);
            }
        }

        public CarteDetailModel? CarteSelection
        {
            get
            {
                return _carteSelection;
            }
            set
            {
                SetProperty(ref _carteSelection, value);
            }
        }
        #endregion
    }
}
