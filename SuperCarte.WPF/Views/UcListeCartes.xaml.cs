﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SuperCarte.WPF.Views
{
    /// <summary>
    /// Logique d'interaction pour UcListesCartes.xaml
    /// </summary>
    public partial class UcListeCartes : UserControl
    {
        public UcListeCartes()
        {
            InitializeComponent();
        }

        private async void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.DataContext != null)
            {
                if (this.DataContext is ListeCartesVM)
                {
                    await ((ListeCartesVM)this.DataContext).ObtenirListeCommande.ExecuteAsync(null);
                }
            }
        }
    }
}
