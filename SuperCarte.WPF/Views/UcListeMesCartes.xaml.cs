﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SuperCarte.WPF.Views;
/// <summary>
/// Logique d'interaction pour UcListeMesCartes.xaml
/// </summary>
public partial class UcListeMesCartes : UserControl
{
    public UcListeMesCartes()
    {
        InitializeComponent();
    }

    private async void UserControl_Loaded(object sender, RoutedEventArgs e)
    {
        if (this.DataContext != null)
        {
            if (this.DataContext is ListeMesCartesVM)
            {
                if (((ListeMesCartesVM)this.DataContext).ObtenirListeCommande != null)
                {
                    await ((ListeMesCartesVM)this.DataContext).ObtenirListeCommande.ExecuteAsync(null);
                }
            }
        }
    }
}
